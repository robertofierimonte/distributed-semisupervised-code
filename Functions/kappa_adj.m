function M = kappa_adj(A)
% Kappa* is a linear operator that maps an Euclidean Distance Matrix (EDM)
% into the space of semidefinite positive matrices. Is the adjoint operator
%of Kappa. For more information, refer to:

%   [1] B. Mishra, G. Meyer and R. Sepulchre - Low-rank optimization for 
%       distance matrix completion.

%   [2] I. J. Schoenberg -  Remarks to maurice fr�echet�s article "sur la 
%       definition axiomatique d�une classe d�espace distanci�es vectoriellement
%       applicable sur l�espace de Hilbert".

    M = 2*(diag(A*ones(size(A,1),1))-A);
end

