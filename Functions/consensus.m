function avg = consensus(X,W,max_iter,sum,ind)
%Consensus is a distributed iterative protocol designed to compute the 
%average of a series of measurements or data within a network. The protocol
%consists in a series of local weighted average between neighbors nodes. If
%the adjacency matrix satisfies particular properties, it is proved that
%the protocol converges (for each agent in the network) to the real average
%of the initial measurements. For more information, refer to:

% Reza Olfati-Saber, J. Alex Fax, and Richard M. Murray - Consensus and 
% Cooperation in Networked Multi-Agent Systems

% Input:

% - A cell array X, composed by L matrices, corresponding to the different
%   data owned by the L agents in the network.
% - A (L by L) adjacency matrix W, whose elements Wij indicate the strength
%   of the connection between agents i and j.
% - An integer indicating the maximum number of iterations before the
%   protocol stops
% - A boolean flag indicating whether the result has to be summed or
%   averaged (0 means to compute average, 1 means to compute sum)
% - (optional) The index ind of an agent to access directly to its solution

% Output:

% - A cell array avg, containing the final values of the data for the L
%   agents or, in case the index is specified, the solution corresponding
%   to the ind-th agent

    L = size(W,1);

    for ii = 1:max_iter
        
        local = X;
        
        for n = 1:L
            
            X{n} = zeros(size(X{n}));
            
            for jj = 1:L
                
                X{n} = X{n} + local{jj}*W(n,jj);
                
            end
        end
    end
    if nargin == 5
        if sum == 1     
            avg = L*X{ind};
        else
            avg = X{ind};
        end
    else
        avg = X;
    end
end