function D = compute_incomplete_EDM(X,N)
%Compute_incomlete_EDM computes an incomplete Euclidean Distance Matrix D 
%from an incomplete set of data X. Elements of D corresponding to missing
%data in X are set equal to 0.

% Input:

% - A matrix of data X, where the first column indicates the index of the 
%   pattern, and the other columns represent the features. 

% - An integer N, which indicates the total number of patterns in X if X 
%   was complete.

% Output:

% - A (N by N) incomplete EDM D, with unknown entries set equals to 0

    d = size(X,2) - 1;
    fullX = zeros(N,d+1);
    known_idx = setdiff([1:N],X(:,1));
    
    fullX(X(:,1),:) = X;
    
    D = squareform(pdist(fullX(:,2:end),'euclidean').^2);
    D(known_idx,:) = 0;
    D(:,known_idx) = 0;
end

