function new_X = data_exchange(t,local_X,p,max_iter)
%Data_exchange is a protocol designed to exchange patterns belonging to
%different agents in a distributed system

% Input:

% - A Network Topology t with agents
% - A cell array local_X of L local datasets corresponding to each agent
% - the fraction p of the number of original pattern to exchange at every
%   iteration
% - the maximum number of iterations

% Output:

% - A cell array of L local updated datasets
    
    shared_data{t.N,1} = []; sent{t.N,1} = []; last_received{t.N,1} = []; new_X = local_X;
    
    for ii = 1:max_iter

        for n = 1:t.N

            if (ii == 1)
                shared_data{n} = datasample(new_X{n},floor(p*size(new_X{n},1)),'Replace',false);
                sent{n} = shared_data{n};
            else
                shared_data{n} = datasample(setdiff(new_X{n},sent{n},'rows'),floor(p*size(setdiff(new_X{n},sent{n},'rows'),1)),'Replace',false);
                sent{n} = union(sent{n},shared_data{n},'rows');
            end
        end

        for n = 1:t.N

            last_received{n} = [];

            for jj = getNeighbors(t, n)

                last_received{n} = vertcat(last_received{n},shared_data{jj});
            end
            new_X{n} = union(new_X{n}, last_received{n},'rows');
        end
    end
end

