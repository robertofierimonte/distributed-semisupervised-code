function Q = diffusion_EDM_completion(t,local_similarity,r,s,max_iter)

%Diffusion_EDM_completion solves the Matrix Completion Problem in a distributed
%fashion. The algorithm is based on an alternation of updating and
%diffusions equation. For more information on the factorization used please
%refer to: 

%   [1] B. Mishra, G. Meyer and R. Sepulchre - Low-rank optimization for 
%       distance matrix completion.

%   [2] I. J. Schoenberg -  Remarks to maurice fr�echet�s article "sur la 
%       definition axiomatique d�une classe d�espace distanci�es vectoriellement
%       applicable sur l�espace de Hilbert".

% For more information about the 'Diffusion Adaptation' framework, please
% refer to:

%   [3] A.H. Sayed - Adaptive Networks.

% Input:

% - A Network Topolgy t
% - A cell array of L incomplete EDMs corresponding to the L agent in the
%   distributed system
% - The rank r of the EDMs
% - The step-size parameter alpha
% - The maximum number of iterations to reach before the algorithm stops

% Output:
% - A cell array Q of L complete EDMs corresponding to the L agents in the
%   distributed system

    A{t.N,1} = []; H{t.N,1} = []; B{t.N,1} = []; Q{t.N,1} = [];
    n_train = size(local_similarity{1},1);
    
    for n = 1:t.N
        A{n} = rand(n_train,r);
        H{n} = logical(local_similarity{n}+eye(n_train));
    end

    for i = 1:max_iter
        for n = 1:t.N
            A{n} = A{n} - 2*s*kappa_adj(H{n}.*(kappa(A{n}*A{n}')-local_similarity{n}))*A{n};
        end
        A = consensus(A,t.W,1,0);
    end
    for n = 1:t.N
        B{n} = A{n}*A{n}';
        Q{n} = kappa(B{n});
        Q{n} = Q{n} + logical(local_similarity{n}).*(local_similarity{n}-Q{n});
    end
end

