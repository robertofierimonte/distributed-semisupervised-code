function K = compute_kernel(X,Y,kernel_type,varargin)
%Compute_kernel computes the kernel matrix corresponding to two sets of data

% Input:

% - A (N1 by d) matrix of data X
% - A (N2 by d) matrix of data Y (can be equal to X)
% - A string that defines the type of kernel
% - A set of optional parameters depending on the type of kernel

% Output:

% - A (N1 by N2) kernel matrix K

    if strcmp(kernel_type,'linear')
        
        K = X*Y';
        
    elseif strcmp(kernel_type,'gaussian')
        
        EDM = pdist2(X,Y,'euclidean').^2;
        
        K = exp(-EDM/(2*varargin{1}^2));
        
    end

end

