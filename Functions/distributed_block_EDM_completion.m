function M = distributed_block_EDM_completion(D,I,W,alpha,r,max_iter)

%Distributed_block_EDM_completion solves the Matrix Completion Problem in a distributed
%fashion, by applying a modified version of the Alternating Direction
%Method (ADM). For more information, see the papers:
% 
%     [1] A.Lin, Q.Ling - Decentralized and Privacy-Preserving Low-Rank
%         Matrix Completion
%     [2] Q.Ling, Y.Xu, W.Yin, Z.Wen - Decentralized Low-Rank Matrix Completion
%
% Input:

% - A cell array D of L incomplete EDMs corresponding to the L agent in the
%   distributed system
% - A cell array I, wherein the k-th cell contains a containing a vertical
%   block of the identity matrix correspoding to the data owned by the k-th
%   agent
% - A (L by L) adjacency matrix W
% - The step-size parameter alpha
% - The rank r of the EDMs
% - The maximum number of iterations to reach before the algorithm stops

% Output:
% - A cell array M of L complete EDMs corresponding to the L agents in the
%   distributed system

    n_nodes = length(W);
    W_t = (eye(n_nodes)+W)/2;

    Omega = cell(n_nodes,1); X = cell(n_nodes,2); Y = cell(n_nodes,2); 
    Z = cell(n_nodes,2);
    
    for ii = 1:n_nodes
        Omega{ii} = logical(D{ii});
        Z{ii,1} = D{ii}.*Omega{ii};
        Y{ii,1} = rand(r,size(D{ii},2));
        X{ii,1} = rand(size(D{ii},1),r);
    end

    for k = 1:max_iter
        
        if k == 1
            
            temp_X = X; temp_Y = Y; temp_Z = Z;
            
            for ii = 1:n_nodes
                X{ii,2} = -alpha*(temp_X{ii}-temp_Z{ii}*temp_Y{ii}');
                
                for jj = 1:n_nodes
                    X{ii,2} = X{ii,2} + W(ii,jj)*temp_X{jj};
                end
            end

        else
            
            temp_X = X(:,2); temp_Y = Y(:,2); temp_Z = Z(:,2);
            old_X = X(:,1); old_Y = Y(:,1); old_Z = Z(:,1);
            
            for ii = 1:n_nodes
               temp_X{ii} = X{ii,2}; temp_Y{ii} = Y{ii,2}; temp_Z{ii} = Z{ii,2};
               old_X{ii} = X{ii,1}; old_Y{ii} = Y{ii,1}; old_Z{ii} = Z{ii,1};
            end
            
            for ii = 1:n_nodes
                
                X{ii,2} = temp_X{ii}-alpha*(temp_X{ii}-old_X{ii}-temp_Z{ii}*temp_Y{ii}'...
                    +old_Z{ii}*old_Y{ii}');
                
                for jj = 1:n_nodes
                    X{ii,2} = X{ii,2} + W(ii,jj)*temp_X{jj} - W_t(ii,jj)*old_X{jj};
                end
                
                X{ii,1} = temp_X{ii}; Y{ii,1} = temp_Y{ii}; Z{ii,1} = temp_Z{ii};
            end
        end
        
        for ii = 1:n_nodes
            Y{ii,2} = (X{ii,2}'*X{ii,2})\(X{ii,2}'*temp_Z{ii});
        end
        
        for ii = 1:n_nodes
            Z{ii,2} = X{ii,2}*Y{ii,2} + (D{ii}-X{ii,2}*Y{ii,2}).*Omega{ii} - (X{ii,2}*Y{ii,2}).*I{ii};
            Z{ii,2} = (Z{ii,2}+abs(Z{ii,2}))/2;
        end
    end
    
    M = cell(n_nodes,1);
    for n = 1:n_nodes
        for ii = 1:n_nodes
            M{n} = [M{n} Z{ii,2}];
        end
    end
    
    for n = 1:n_nodes
        M{n} = (M{n}+M{n}')/2;
    end
end

