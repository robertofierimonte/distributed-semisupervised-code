function new_similarity = points_exchange(t,local_similarity,p,max_iter)
%Points_exchange is a protocol designed to exchange entries belonging to
%Euclidean Distance Matrices belonging to different agents in a distributed
%system

% Input:

% - A Network Topology t with agents
% - A cell array local_X of L local EDMs corresponding to each agent
% - the fraction p of the number of original entries of the matrices to 
%   exchange at every iteration
% - the maximum number of iterations

% Output:

% - A cell array of L local updated EDMs

    shared_point{t.N,1} = []; new_similarity = local_similarity;
    
    for ii = 1:max_iter
        for n = 1:t.N
            
            if ii == 1
                all = find(new_similarity{n});
                ind = randsample(all,ceil(p*length(all)));
                shared_point{n} = [ind new_similarity{n}(ind)];
            else
                all = setdiff(find(new_similarity{n}),shared_point{n}(:,1));
                ind = randsample(all,ceil(p*length(all)));
                shared_point{n} = [ind new_similarity{n}(ind)];
            end
        end

        for n = 1:t.N
            for jj = getNeighbors(t, n)

                new_similarity{n}(shared_point{jj}(:,1)) = shared_point{jj}(:,2);            
            end
        end
    end
    
end

