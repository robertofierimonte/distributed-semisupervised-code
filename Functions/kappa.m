function M = kappa(A)
% Kappa is a linear operator that maps a semidefinite positive matrix into
% the space of the Euclidean Distance Matrices (EDMs). For more
% information, refer to:

%   [1] B. Mishra, G. Meyer and R. Sepulchre - Low-rank optimization for 
%       distance matrix completion.

%   [2] I. J. Schoenberg -  Remarks to maurice fr�echet�s article "sur la 
%       definition axiomatique d�une classe d�espace distanci�es vectoriellement
%       applicable sur l�espace de Hilbert".

    M = diag(A)*ones(size(A,1),1)'+ ones(size(A,1),1)*diag(A)'-2*A;
end

