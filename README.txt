Distr-LapRLS MATLAB code

  Description
  -----------

  This is a serial implementation in MATLAB of the algorithm presented in:
  
  Fierimonte R., Scardapane S., Uncini A. and M. Panella - Fully
  Decentralized Semi-supervised Learning via Privacy-Preserving 
  Matrix Completion.

  The paper describing the code is currently under review at IEEE Transactions
  on Neural Networks and Learning Systems.

  The proposed algorithm extends the LapRLS algorithm to distributed learning
  where the training set is distrubuted through a network of agents, each of
  those has access only to few patterns. The Laplacian matrix used to store
  information about the patterns' similarity is computed using a novel
  distributed Euclidan Distance Matrix (EDM) completion algorithm.


  Documentation
  -------------

  A demo file to perform simulations is provided. All the code is commented
  with appropriate instructions and references where needed.
  

  Contacts
  --------

  If you have any request, bug report, or inquiry, you can contact
  the author at roberto [dot] fierimonte [at] gmail [dot] com.