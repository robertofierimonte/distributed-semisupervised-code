dataset = Dataset(name, X, task, Y);    % Load and preprocess dataset
dataset = shuffle(dataset, n_run);
dataset = normalize(dataset,-1,1);
dataset = generateNPartitions(dataset, n_run, ExactPartition(size(dataset.X,1)-N_test, N_test), ExactPartition(size(dataset.X,1)-N_test-N_l, N_l));

iter = 0:10:150;
reverseStr = ''; steps = n_run*length(iter);

time1(n_run,length(iter)) = 0;  err1(n_run,length(iter)) = 0;
time2(n_run,length(iter)) = 0;  err2(n_run,length(iter)) = 0;

Kerr1(n_run,length(iter)) = 0;  Kerr2(n_run,length(iter)) = 0;
Lerr1(n_run,length(iter)) = 0;  Lerr2(n_run,length(iter)) = 0;

for p = 1:n_run
    
    [trainData, ~] = getFold(dataset,p);
    [N, d] = size(trainData.X);
    trainData = distributeDataset(trainData, KFoldPartition(L));
    
    EDM = squareform(pdist(trainData.X,'euclidean').^2);
    Lap = laplacian(options,trainData.X);                    % Compute the Laplacian matrix
    Ker = compute_kernel(trainData.X,trainData.X,'gaussian',options.GraphWeightParam);   % Compute the global kernel matrix

    localDataset = cell(L,1);
    
    for n = 1:L

        [localDataset{n}, localIdx{n}] = getLocalPart(trainData, n);

        local_X{n} = [localIdx{n} localDataset{n}.X];
    end

    for ij = iter

        step = (p-1)*length(iter)+find(iter == ij);
        
        new_X = data_exchange(t,local_X,p1,ij);

        full_X = cell(L,1); local_similarity = cell(L,1);
        
        for n = 1:L
           
            full_X{n}(new_X{n}(:,1),:) = new_X{n};

            local_similarity{n} = squareform(pdist(full_X{n}(:,2:end),'euclidean').^2);
            local_similarity{n}(setdiff(1:N,new_X{n}(:,1)),:) = 0;
            local_similarity{n}(:,setdiff(1:N,new_X{n}(:,1))) = 0;
        end
        
        new_similarity = points_exchange(t,local_similarity,p2,ij);

        for n = 1:L
            new_similarity{n} = new_similarity{n}+((new_similarity{n}~=new_similarity{n}').*new_similarity{n}');
        end
    % Step 5. Implement an algorithm to complete the the euclidean distance matrix from the available entries
    %
        D = cell(L,1); I = cell(L,1);
        
        r = min(N, d+2);
        ind = 0;
        Id = eye(N);
        for n = 1:L
            D{n} = new_similarity{n}(:,ind+1:ind+trainData.distrPartition.partition_struct.TestSize(n));
            I{n} = Id(:,ind+1:ind+trainData.distrPartition.partition_struct.TestSize(n));
            ind = ind +  trainData.distrPartition.partition_struct.TestSize(n);
        end

    % Step 6. Each node complete its own distance matrix using the algorithm defined in Step. 5
    %
        tic;
        EDM1 = distributed_block_EDM_completion(D,I,t.W,0.4,r,1500);
        time1(p,find(iter == ij)) = toc;
        err1(p,find(iter == ij)) = norm(EDM-EDM1{1},'fro')/norm(EDM,'fro');
        
        tic;
        Q = diffusion_EDM_completion(t,new_similarity,r,10^-6,1500);
        EDM2 = Q{1};
        time2(p,find(iter == ij)) = toc;
        err2(p,find(iter == ij)) = norm(EDM-EDM2,'fro')/norm(EDM,'fro');
        
        distr_Ker1 = exp(-EDM1{1}/(2*options.GraphWeightParam^2));
        distr_Lap1 = laplacian_by_W(options,EDM1{1});
        Kerr1(p,find(iter == ij)) = norm(distr_Ker1-Ker,'fro')/norm(Ker,'fro');
        Lerr1(p,find(iter == ij)) = norm(distr_Lap1-Lap,'fro')/norm(Lap,'fro');
        
        distr_Ker2 = exp(-EDM2/(2*options.GraphWeightParam^2));
        distr_Lap2 = laplacian_by_W(options,EDM2);
        Kerr2(p,find(iter == ij)) = norm(distr_Ker2-Ker,'fro')/norm(Ker,'fro');
        Lerr2(p,find(iter == ij)) = norm(distr_Lap2-Lap,'fro')/norm(Lap,'fro');
        
        percentDone = 100 * step/steps;
        msg1 = sprintf('Percent done: %3.1f', percentDone);
        fprintf([reverseStr, msg1]);
        reverseStr = repmat(sprintf('\b'), 1, length(msg1));
    end
end
clc; fprintf('Simulation done\n');
plot_EDMerr;