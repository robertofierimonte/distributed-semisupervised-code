
% General properties
width = 3;                   % Width in inches
height = 1.2;                   % Height in inches
font_size = 8;                  % Fontsize
font_size_leg = 8;              % Font size (legend)
font_name = 'TimesNewRoman';    % Font name
line_width = 0.4;                 % LineWidth

% Plot instructions
val = [mean(mean(time1)); mean(mean(time2))]/L;
figure();
figshift;
% barh(val, line_width)
barh(1, val(1),line_width, 'FaceColor',[139/255,0,0]);
hold on;
barh(2, val(2),line_width, 'FaceColor',[0,0,139/255]);

% Set various properties
set(gca,'Ytick',1:2,'YTickLabel',{'DBE', 'DGD'});
% yTicks = get(gca,'ytick');
% xTicks = get(gca, 'xtick');
% minX = min(xTicks);
% % You will have to adjust the offset based on the size of figure
% VerticalOffset = 0;
% if strcmp(name, 'g50c')
%     HorizontalOffset = 1.05;
% elseif strcmp(name, 'BCI')
%     HorizontalOffset = 0.6;
% elseif strcmp(name, 'twomoons')
%     HorizontalOffset = 0.17;
% else
%     HorizontalOffset = 43;
% end
% text(minX - HorizontalOffset, yTicks(1) - VerticalOffset, ['$$\begin{array}{l}','\rm Decentralized\\' '\rm Block\\' '\rm Estimation' ,'\end{array}$$'], 'Interpreter', 'latex','FontSize', 5, 'FontName', font_name)
% text(minX - HorizontalOffset, yTicks(2) - VerticalOffset, ['$$\begin{array}{l}','\rm Diffusion\\' '\rm Gradient\\' '\rm Descent' ,'\end{array}$$'], 'Interpreter', 'latex','FontSize', 5, 'FontName', font_name)

box on;
grid on;

xlabel('EDM completion time [s]', 'FontSize', font_size, 'FontName', font_name);
ylabel('Algorithm','FontSize', font_size, 'FontName', font_name);

set(gca, 'FontSize', font_size);
set(gca, 'FontName', font_name);

% Set the default Size for display
set(gcf, 'PaperUnits', 'inches');
defpos = get(gcf, 'Position');
set(gcf,'Position', [defpos(1) defpos(2) width*100, height*100]);

% Set the defaults for saving/printing to a file
defsize = get(gcf, 'PaperSize');
left = (defsize(1)- width)/2;
bottom = (defsize(2)- height)/2;
defsize = [left+1000, bottom, width, height];
set(gcf, 'PaperPosition', defsize);

% Export the figure
export_fig(strcat('EDM_time_',name), '-pdf', '-eps', '-painters','-transparent')