% Distributed SSL Simulation
%
% Input:
%
% - An undirected graph G(N,E) which represent the distributed system
% - A distributed dataset D, with both labeled and unlabeled patterns
% - The model of the learning machine
% - Params for all the algorithms
% 
% Output:
% 
% - The global solution vector
% - Test error for Centralized, Distributed and Local models

% Start simulation
dataset = Dataset(name, X, task, Y);
dataset = shuffle(dataset, n_run);
dataset = normalize(dataset, -1, 1);
dataset = generateNPartitions(dataset, n_run, ExactPartition(size(dataset.X,1)...
    -N_test, N_test), ExactPartition(size(dataset.X,1)-N_test-N_l, N_l));

locErr(n_run,L) = 0; locTime(n_run,L) = 0;
centrErr(n_run) = 0; centrTime(n_run) = 0;
distrErr(n_run) = 0; distrTime(n_run) = 0;
complErr(n_run,L) = 0;  complTime(n_run) = 0;

reverseStr = ''; steps = n_run;

for p = 1:n_run
    step = p;
    
    Alg = LaplacianRLS(lambda, gamma);
    
    [trainData, testData, ssInd] = getFold(dataset,p);
    [N,d] = size(trainData.X);
    r = min(N,d+2);
    
    % Centr-RLS
    trainData = distributeDataset(trainData, KFoldPartition(L));
    tic;
    EDM = squareform(pdist(trainData.X,'euclidean').^2);
    Lap = laplacian(options,trainData.X);                    % Compute the Laplacian matrix
    Ker = compute_kernel(trainData.X,trainData.X,'gaussian',...
        options.GraphWeightParam);   % Compute the global kernel matrix
    
    centrTime(p) = toc;
    
    centrErr(p) = Alg.train(trainData, ssInd, Lap, Ker).test...
        (testData, trainData, options).test_error;

    localDataset = cell(L,1);
    
    % Compute the local solutions
    for n = 1:L

        [localDataset{n}, localIdx{n}, local_ssInd{n}] = getLocalPart(trainData, n, ssInd);
        
        tic;
        local_Lap = laplacian(options,localDataset{n}.X);                   
        local_Ker = compute_kernel(localDataset{n}.X,localDataset{n}.X,...
            'gaussian',options.GraphWeightParam);

        locErr(p,n) = Alg.train(localDataset{n}, local_ssInd{n}, local_Lap,...
            local_Ker).test(testData, localDataset{n}, options).test_error;
        locTime(p,n) = toc;
        
        local_X{n} = [localIdx{n} localDataset{n}.X];   
    end
    tic;
    new_X = data_exchange(t,local_X,p1,max_iter1);

    full_X = cell(L,1); local_similarity = cell(L,1);
    for n = 1:L

        local_similarity{n} = compute_incomplete_EDM(new_X{n},N);

    end

    new_similarity = points_exchange(t,local_similarity,p2,max_iter2);

    for n = 1:L
        new_similarity{n} = new_similarity{n}+((new_similarity{n}~=new_similarity{n}').*new_similarity{n}');
    end
    
    tic;
    Q = diffusion_EDM_completion(t,new_similarity,r,step_size,max_iter3);
    complTime(p) = toc;

    J = cell(L,1);
    for n = 1:L
        complErr(p,n) = norm(EDM-Q{n},'fro')/norm(EDM,'fro');
        J{n} = ismember([1:N],intersect(localIdx{n},find(ssInd)))';
    end
    J_tot = diag(consensus(J,t.W,50,1,1));
    
    Y_til = cell(L,1); partial = cell(L,1);
    
    Ker_distr = cell(L,1); Lap_distr = cell(L,1);
    for n = 1:L
        Ker_distr{n} = exp(-Q{n}/(2*options.GraphWeightParam^2));
        Lap_distr{n} = laplacian_by_W(options,Q{n});
    end
    
    if strcmp(dataset.task,'MC')
        Y_dummy = dummyvar(trainData.Y);
    end
    
    for n = 1:L
        Y_til{n} = zeros(N,max(trainData.Y));
        if strcmp(dataset.task,'BC')
            Y_til{n}(intersect(localIdx{n},find(ssInd)),:) = localDataset{n}.Y(find(local_ssInd{n}),:);
        elseif strcmp(dataset.task,'MC')
            Y_til{n}(intersect(localIdx{n},find(ssInd)),:) = Y_dummy(intersect(localIdx{n},find(ssInd)),:);
        end
        partial{n} = (J_tot*Ker_distr{n}+lambda*eye(N)+gamma*Lap_distr{n}*Ker_distr{n})\Y_til{n};
    end
    
    distributed = consensus(partial,t.W,100,1,1);
    
    if strcmp(dataset.task,'BC')       
        Y_est_distr = sign(compute_kernel(trainData.X,testData.X,'gaussian',options.GraphWeightParam)'*distributed);
        distrErr(p) = sum(Y_est_distr ~= testData.Y)/N_test;
    elseif strcmp(dataset.task,'MC')
        Y_est_distr = (vec2ind((compute_kernel(trainData.X,testData.X,'gaussian',options.GraphWeightParam)'*distributed)'))';
        distrErr(p) = sum(Y_est_distr ~= testData.Y)/N_test;
    end
    distrTime(p) = toc;
    
    percentDone = 100 * step/steps;
    msg1 = sprintf('Percent done: %3.1f', percentDone);
    fprintf([reverseStr, msg1]);
    reverseStr = repmat(sprintf('\b'), 1, length(msg1));
end
clc;
fprintf('Simulation done: dataset %s, %u labeled data, %u unlabeled data, %u test data, %u runs\n', name, N_l, N-N_l, N_test, n_run);
fprintf('---------------------------------------------------------------\n');
fprintf('Misclassification error for tested algorithms (average +- std)\n');
fprintf('Centr-LapRLS: %.4f%% (+- %.4f)\n',mean(centrErr), std(centrErr));
fprintf('Distr-LapRLS: %.4f%% (+- %.4f)\n',mean(distrErr),std(distrErr));
fprintf('Local-LapRLS: %.4f%% (+- %.4f)\n',mean(reshape(locErr,L*n_run,1)),std(reshape(locErr,L*n_run,1)));
fprintf('\nEDM Completion statistics (average +- std)\n');
fprintf('Completion Error:  %.4f (+- %.4f)\n',mean(reshape(complErr,L*n_run,1)),std(reshape(complErr,L*n_run,1)));
fprintf('Completion Time:   %.4fs (+- %.4f)\n',mean(complTime./L),std(complTime./L));

