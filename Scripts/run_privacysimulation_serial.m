% Distributed SSL Simulation with privacy-preserving
%
% Input:
%
% - An undirected graph G(N,E) which represent the distributed system
% - A distributed dataset D, with both labeled and unlabeled patterns
% - The model of the learning machine
% - Params for all the algorithms
% 
% Output:
% 
% - The global solution vector
% - Test error for the distributed privacy-preserving strategies
%
% Start simulation

dataset = Dataset(name, X, task, Y);
dataset = shuffle(dataset, n_run);
dataset = normalize(dataset, -1, 1);
dataset = generateNPartitions(dataset, n_run, ExactPartition(size(dataset.X,1)...
    -N_test, N_test), ExactPartition(size(dataset.X,1)-N_test-N_l, N_l));

reverseStr = ''; proj = 0.1:0.05:0.95; 
% proj = 0.1;

linClassErr(n_run,length(proj)) = 0; linTime(n_run,length(proj)) = 0;
nLinClassErr(n_run,length(proj)) = 0; nLinTime(n_run,length(proj)) = 0;
linComplErr(n_run,L,length(proj)) = 0; nLinComplErr(n_run,L,length(proj)) = 0;

steps = n_run*length(proj);

for p = 1:n_run
    
    [trainData, testData, ssInd] = getFold(dataset,p);
    [N,d] = size(trainData.X);
    trainData = distributeDataset(trainData, KFoldPartition(L));
    
    D = squareform(pdist(trainData.X,'euclidean').^2);
    
    for jj = proj
        
        step = (p-1)*length(proj)+find(proj == jj);
        
        m = floor(d*jj);
        
        R = sqrt(sigma).*randn(d,m);

        linTrain = trainData;
        
        linTest = testData;
        
        linTrain.X = trainData.X*R/sqrt(m*sigma);
        
        linTest.X = testData.X*R/sqrt(m*sigma);
        
        B = sqrt(sigmaB).*randn(m,1);
        E = sqrt(sigmaE).*randn(m,h);
        A = sqrt(sigmaA).*randn(h,1);
        W = sqrt(sigmaW).*randn(h,d);

        nonLinTrain = trainData;
        
        nonLinTest = testData;
        
        nonLinTrain.X = repmat(B',N,1)+tanh(repmat(A',N,1) +trainData.X*W')*E';
        
        nonLinTest.X = repmat(B',N_test,1)+tanh(repmat(A',N_test,1) +testData.X*W')*E';
        
        localDataset = cell(L,2);
    
        linX = cell(L,1); nLinX = cell(L,1);
        
        % Compute the local solutions
        for n = 1:L

            [localDataset{n,1}, localIdx{n}, local_ssInd{n}] = getLocalPart(linTrain, n, ssInd);
            
            [localDataset{n,2}, ~] = getLocalPart(nonLinTrain,n);

            linX{n} = [localIdx{n} localDataset{n,1}.X];
            
            nLinX{n} = [localIdx{n} localDataset{n,2}.X];
        end

        linXnew = data_exchange(t,linX,p1,max_iter1);
        
        nLinXnew = data_exchange(t,nLinX,p1,max_iter1);

        linLocal_similarity = cell(L,1); nLinLocal_similarity = cell(L,1);
        
        for n=1:L
            linLocal_similarity{n} = compute_incomplete_EDM(linXnew{n},N);
            
            nLinLocal_similarity{n} = compute_incomplete_EDM(nLinXnew{n},N);
        end

        linNew_similarity = points_exchange(t,linLocal_similarity,p2,max_iter2);
        
        nLinNew_similarity = points_exchange(t,nLinLocal_similarity,p2,max_iter2);

        for n = 1:L
            linNew_similarity{n} = linNew_similarity{n}+...
                ((linNew_similarity{n}~=linNew_similarity{n}').*linNew_similarity{n}');
            
            nLinNew_similarity{n} = nLinNew_similarity{n}+...
                ((nLinNew_similarity{n}~=nLinNew_similarity{n}').*nLinNew_similarity{n}');
        end

        r = min(N,m+2);

        linD = diffusion_EDM_completion(t,linNew_similarity,r,step_size,max_iter3);
        nLinD = diffusion_EDM_completion(t,nLinNew_similarity,r,step_size,max_iter3);

        J = cell(L,1);
        for n = 1:L
            linComplErr(p,n,find(proj == jj)) = norm(linD{n}-D,'fro')/norm(D,'fro');
            
            nLinComplErr(p,n,find(proj == jj)) = norm(nLinD{n}-D,'fro')/norm(D,'fro');
            
            J{n} = ismember([1:N],intersect(localIdx{n},find(ssInd)))';
        end
        J_tot = diag(consensus(J,t.W,50,1,1));

        Y_til = cell(L,1); partial = cell(7,2);

        linKer = cell(L,1); linLap = cell(L,1);
        
        nLinKer = cell(L,1); nLinLap = cell(L,1);
        
        for n = 1:L
            linKer{n} = exp(-linD{n}/(2*options.GraphWeightParam^2));
            
            linLap{n} = laplacian_by_W(options,linD{n});
            
            nLinKer{n} = exp(-nLinD{n}/(2*options.GraphWeightParam^2));
            
            nLinLap{n} = laplacian_by_W(options,nLinD{n});
        end

        if strcmp(dataset.task,'MC')
            Y_dummy = dummyvar(trainData.Y);
        end

        linPartial = cell(L,1); nLinPartial = cell(L,1);
        
        for n = 1:L
            Y_til{n} = zeros(N,max(trainData.Y));
            Y_til_bin{n} = zeros(N,1);
            
            if strcmp(dataset.task,'BC')
                Y_til{n}(intersect(localIdx{n},find(ssInd)),:) = localDataset{n}.Y(find(local_ssInd{n}),:);
                
            elseif strcmp(dataset.task,'MC')                
                Y_til{n}(intersect(localIdx{n},find(ssInd)),:) = Y_dummy(intersect(localIdx{n},find(ssInd)),:);
                Y_bin{n} = sign(localDataset{n}.Y-10.5);
                Y_til_bin{n}(intersect(localIdx{n},find(ssInd)),:) = Y_bin{n}(find(local_ssInd{n}),:);
                
                linPartialBin{n} = (J_tot*linKer{n}+lambda*eye(N)+gamma*linLap{n}*linKer{n})\Y_til_bin{n};
                nLinPartialBin{n} = (J_tot*nLinKer{n}+lambda*eye(N)+gamma*nLinLap{n}*nLinKer{n})\Y_til_bin{n};
            end
            
            linPartial{n} = (J_tot*linKer{n}+lambda*eye(N)+gamma*linLap{n}*linKer{n})\Y_til{n};
            
            nLinPartial{n} = (J_tot*nLinKer{n}+lambda*eye(N)+gamma*nLinLap{n}*nLinKer{n})\Y_til{n};
            
        end

        linBeta = consensus(linPartial,t.W,100,1,1);
        nLinBeta = consensus(nLinPartial,t.W,100,1,1);
        
        if strcmp(dataset.task,'MC')
            linBetaBin = consensus(linPartialBin,t.W,100,1,1);
            nLinBetaBin = consensus(nLinPartialBin,t.W,100,1,1);
        end

        if strcmp(dataset.task,'BC')       
            linY_est = sign(compute_kernel(linTrain.X,linTest.X,...
                'gaussian',options.GraphWeightParam)'*linBeta);
            
            nLinY_est = sign(compute_kernel(nonLinTrain.X,nonLinTest.X,...
                'gaussian',options.GraphWeightParam)'*nLinBeta);
            
            linClassErr(p,find(proj == jj)) = sum(linY_est ~= testData.Y)/N_test;
            
            nLinClassErr(p,find(proj == jj)) = sum(nLinY_est ~= testData.Y)/N_test;
            
        elseif strcmp(dataset.task,'MC')
            linY_est = (vec2ind((compute_kernel(linTrain.X,linTest.X,...
                'gaussian',options.GraphWeightParam)'*linBeta)'))';
            
            nLinY_est = (vec2ind((compute_kernel(nonLinTrain.X,nonLinTest.X,...
                'gaussian',options.GraphWeightParam)'*nLinBeta)'))';
            
            linClassErr(p,find(proj == jj)) = sum(linY_est ~= testData.Y)/N_test;
            
            nLinClassErr(p,find(proj == jj)) = sum(nLinY_est ~= testData.Y)/N_test;
            
            linY_estBin = sign(compute_kernel(linTrain.X,linTest.X,...
                'gaussian',options.GraphWeightParam)'*linBetaBin);
            
            nLinY_estBin = sign(compute_kernel(nonLinTrain.X,nonLinTest.X,...
                'gaussian',options.GraphWeightParam)'*nLinBetaBin);
            
            linClassErrBin(p,find(proj == jj)) = sum(linY_estBin ~= sign(testData.Y-10.5))/N_test;
            
            nLinClassErrBin(p,find(proj == jj)) = sum(nLinY_estBin ~= sign(testData.Y-10.5))/N_test;
        end
        
        percentDone = 100 * step/steps;
        msg1 = sprintf('Percent done: %3.1f', percentDone);
        fprintf([reverseStr, msg1]);
        reverseStr = repmat(sprintf('\b'), 1, length(msg1));
    end
end
clc;
fprintf('Simulation done');

