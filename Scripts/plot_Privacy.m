
% General properties
width = 3.45;                   % Width in inches
height = 2.6;                   % Height in inches
font_size = 8;                  % Fontsize
font_size_leg = 8;              % Font size (legend)
font_name = 'TimesNewRoman';    % Font name
line_width = 1;                 % LineWidth

% Plot instructiond
up = baseline + devst;
low = baseline - devst;
figure();

figshift;
linear = plot(proj,mean(linClassErr,1),'r','LineWidth',line_width);
hold on
nonlinear = plot(proj,mean(nLinClassErr,1),'b--','LineWidth',line_width);
average = plot(proj,repmat(baseline,length(proj),1),'k--','LineWidth',line_width);
confidence = plot(proj,repmat(up,length(proj),1),'k-.','LineWidth',line_width);
plot(proj,repmat(low,length(proj),1),'k-.','LineWidth',line_width);
% Set various properties
% xlim([0 iter(end)]);
ylim([0.46 0.59]);

box on;
grid on;

xlabel('m/d [%]', 'FontSize', font_size, 'FontName', font_name);
ylabel('Classification Error [%]', 'FontSize', font_size, 'FontName', font_name);

set(gca, 'FontSize', font_size);
set(gca, 'FontName', font_name);

h_legend = legend([linear, nonlinear, average, confidence],'Linear Transformation','Nonlinear Transformation','Baseline',...
     'Baseline \pm St.Dev.', 'Interpreter','latex', 'Location', 'NorthEast');
set(h_legend,'FontSize', font_size_leg);
set(h_legend,'FontName', font_name);

% Set the default Size for display
set(gcf, 'PaperUnits', 'inches');
defpos = get(gcf, 'Position');
set(gcf,'Position', [defpos(1) defpos(2) width*100, height*100]);

% Set the defaults for saving/printing to a file
defsize = get(gcf, 'PaperSize');
left = (defsize(1)- width)/2;
bottom = (defsize(2)- height)/2;
defsize = [left, bottom, width, height];
set(gcf, 'PaperPosition', defsize);

% Export the figure
export_fig(strcat('Privacy_error_',name), '-pdf', '-eps', '-painters','-transparent')