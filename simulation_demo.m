%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%          Configuration file for Distributed SSL simulations             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all; clear all; clc; rng(1);
fprintf('Welcome, starting distributed semisupervised learning simulation\n');

load ('topology');
load('g50c');
    
%Params for g50c
N_test = 186;                                % Number of test data
N_l = 50;                                    % Number of traing labeled data

options.NN = 50;                             % Number of Nearest Neighbors
options.GraphDistanceFunction = 'euclidean'; % Type of distance function
options.GraphWeights = 'heat';               % Type of kernel
options.GraphWeightParam = 17.5;             % Param for kernel
options.LaplacianNormalize = 1;              % Flag for Laplacian normalization
options.LaplacianDegree = 5;                 % Value for iterated Laplacian

lambda = 10^-6;                              % Regularization parameter
gamma = 10^-2;                               % Manifold regularization parameter

step_size = 10^-6; max_iter3 = 1500;         % Params for Diffusion Gradient EDM completion

sigma = 1;                                   % Variance for Random-Projection algorithm
h = 20000; sigmaB = 1; sigmaE = 1;           % Inner projection space dimension and variances
sigmaA = 0; sigmaW = 1.1*10^-6;              % values for the non-linear projection algorithm 

p1 = 0.02; max_iter1 = 150;                  % Params for pattern and point exchange
p2 = 0.025; max_iter2 = 150;


% Params for Simulation
n_run = 5;                                     % Number of run
L = 7;                                         % Number of nodes
setPrivacy = 0;                                % Flag for privacy-preserving similarity computation

% To compare the distr-LapRLS algorithm with the centralized and the local
% implementations, choose this simulation
run_SSLsimulation_serial;

% To compare the two algorithms for distributed EDM completion, choose this
% simulation
% run_EDMsimulation_serial;