classdef LaplacianRLS < LearningAlgorithm
    
    properties
        beta; % Weights vector
        test_error; % Algorithm error computed on the test set
        Y_est; % Vector of predicted test output
        train_time; % Training time
    end
    
    methods
        function obj = LaplacianRLS(lambda, gamma)
            obj = obj@LearningAlgorithm('LaplacianRLS');
            obj.params.lambda = lambda;
            obj.params.gamma = gamma;
        end
        
        function obj = train(obj, Dataset, ssInd, Lap, Ker)
            tic;
            [N,~] = size(Dataset.X);
            J = diag(ssInd);
            if strcmp(Dataset.task,'BC')
                Y = Dataset.Y.*ssInd;
            elseif strcmp(Dataset.task,'MC')
                Y = dummyvar(Dataset.Y).*repmat(ssInd,1,size(dummyvar(Dataset.Y),2));
            end

            obj.beta = (J*Ker + obj.params.lambda*eye(N)+obj.params.gamma*Lap*Ker)\Y;
            obj.train_time = toc;
        end
        
        function obj = test(obj, testData, trainData, options)
            if strcmp(trainData.task,'BC')
                obj.Y_est = sign(compute_kernel(trainData.X,testData.X,'gaussian',options.GraphWeightParam)'*obj.beta);
                obj.test_error = sum(obj.Y_est ~= testData.Y)/size(testData.Y,1);
            elseif strcmp(trainData.task,'MC')
                obj.Y_est = (vec2ind((compute_kernel(trainData.X,testData.X,'gaussian',options.GraphWeightParam)'*obj.beta)'))';
                obj.test_error = sum(obj.Y_est ~= testData.Y)/size(testData.Y,1);
            end
        end
    end
    
end

